
import fs from 'fs'
import path from 'path'

import PGAura from 'zunzun/pg-aura/index.mjs'

export default class PG {
  static async start(sm, cfg) {
    try {
      const pg_cfg = JSON.parse(fs.readFileSync(path.resolve(
        process.cwd(), cfg.custom_env || '.pg-env.json'
      )));
      return await PGAura.connect({
        database: pg_cfg.db_name,
        user: pg_cfg.db_user,
        pass: pg_cfg.db_pwd
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
